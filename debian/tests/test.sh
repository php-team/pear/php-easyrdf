#!/bin/sh

set -e

# Start a web server to serve the file.
php --server localhost:10101 --docroot debian/tests/data &
SERVER_PID=$!
php debian/tests/test.php $@
kill -9 $SERVER_PID
