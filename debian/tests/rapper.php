<?php

include_once 'EasyRdf/autoload.php';

EasyRdf\Format::registerSerialiser('dot', 'EasyRdf\Serialiser\Rapper');

$foaf = new \EasyRdf\Graph('http://localhost:10101/foaf.rdf');
$foaf->load();

$data = $foaf->serialise('dot');
if (!is_scalar($data)) {
  exit(1);
}
echo $data, PHP_EOL;
